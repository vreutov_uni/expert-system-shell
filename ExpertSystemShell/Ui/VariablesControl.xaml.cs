﻿using ExpertSystemShell.Model;
using ExpertSystemShell.Ui.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExpertSystemShell.Ui
{
    /// <summary>
    /// Interaction logic for VariablesControl.xaml
    /// </summary>
    public partial class VariablesControl : UserControl
    {
        private ExpertSystem expertSystem;

        public ExpertSystem ExpertSystem
        {
            get { return expertSystem; }
            set
            {
                expertSystem = value;
                lvVariables.ItemsSource = expertSystem.Variables;
            }
        }

        public VariablesControl()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            VariableDialog dlg = new VariableDialog(ExpertSystem)
            {
                Owner = Application.Current.MainWindow
            };
            dlg.ShowDialog();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            OnEditVariable();
        }

        private void OnEditVariable()
        {
            VariableDialog dlg = new VariableDialog(ExpertSystem, lvVariables.SelectedItem as Variable)
            {
                Owner = Application.Current.MainWindow
            };
            dlg.ShowDialog();
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (!ExpertSystem.Remove(lvVariables.SelectedItem as Variable))
            {
                var confirmResult = Xceed.Wpf.Toolkit.MessageBox.Show("Удаление переменной приведет к удалению всех правил, в которых она используется. Вы уверены, что хотите удалить переменную?",
                                     "Подтверждение", MessageBoxButton.YesNo);

                if (confirmResult == MessageBoxResult.Yes)
                {
                    ExpertSystem.Remove(lvVariables.SelectedItem as Domain, true);
                }
            }
        }

        private void lvVariables_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (((FrameworkElement) e.OriginalSource).DataContext != null)
            {
                OnEditVariable();
            }
        }
    }
}
