﻿using ExpertSystemShell.Model;
using ExpertSystemShell.Ui.Dialogs.Consultation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExpertSystemShell.Ui.Dialogs
{
    /// <summary>
    /// Interaction logic for ConsultationDialog.xaml
    /// </summary>
    public partial class ConsultationDialog : Window
    {
        enum ConsultationState { ChoosingGoal, ReqestingVariable };
        ConsultationState State;

        ExpertSystem ExpertSystem { get; set; }

        LIM lim
		{
			get
			{
				return ExpertSystem.LIM;
			}
			set
			{
				ExpertSystem.LIM = value;
			}
		}

        ManualResetEvent valueGetEvent;
        DomainValue currentValue;

        public ConsultationDialog(ExpertSystem expertSystem)
        {
            ExpertSystem = expertSystem;
            valueGetEvent = new ManualResetEvent(false);

            InitializeComponent();

            if (ExpertSystem.Variables.Any(x => x.Type == VariableType.Deducible))
            {
                StartNewConsultation();
            }
            else
            {
                AddMessage(new ConsultationMessage(MessageType.Error,
                    "Невозможно начать консультацию:" +
                    "\nЭС не содержит выводимых переменных"));
                SetUiEnabled(false);
            }
        }

        private void StartNewConsultation()
        {
            InitializeLIM();

            State = ConsultationState.ChoosingGoal;

            spMessages.Children.Clear();

            AddMessage(new ConsultationMessage(MessageType.Question,
                "Выберите цель консультации"));

            cbAnswers.ItemsSource = ExpertSystem.Variables.Where(x => x.Type == VariableType.Deducible);
            
            if (ExpertSystem.GoalVariable != null)
                cbAnswers.SelectedItem = ExpertSystem.GoalVariable;
            else if (cbAnswers.Items.Count > 0)
                cbAnswers.SelectedIndex = 0;

            SetUiEnabled(true);

			grCombo.Visibility = Visibility.Visible;
			grButtons.Visibility = Visibility.Collapsed;
        }

        private void InitializeLIM()
        {
            if (lim != null)
            {
                lim.VariableRequested -= Lim_VariableRequested;
                lim.VariableDeduced -= Lim_VariableDeduced;
            }

            lim = new LIM(ExpertSystem);
            lim.VariableRequested += Lim_VariableRequested;
            lim.VariableDeduced += Lim_VariableDeduced;
        }

        private void Lim_VariableDeduced(object sender, VariableDeducedEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(
                new Action(() =>
                {
                    if (e.HasError)
                    {
                        AddMessage(new ConsultationMessage(MessageType.Error,
                            "Ошибка вывода:\n" +
                            e.Error));
                    }
                    else if (e.Value == null)
                    {
                        AddMessage(new ConsultationMessage(MessageType.Error,
                            "Цель консультации не была достигнута.\nОбратитесь к другой ЭС."));
                    }
                    else
                    {
                        AddMessage(new ConsultationMessage(MessageType.Result,
                            "Цель консультации достигнута!" +
                            "\nРезультат:" +
                            $"\n{ExpertSystem.GoalVariable.Name} - {e.Value.Name}"));
                    }

					grCombo.Visibility = Visibility.Collapsed;
					grButtons.Visibility = Visibility.Visible;
				}));
        }

        private void SetUiEnabled(bool enabled)
        {
            cbAnswers.IsEnabled = enabled;
            btAnswer.IsEnabled = enabled;
        }

        private void Lim_VariableRequested(object sender, VariableRequestEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(
                new Action(() =>
                {
                    State = ConsultationState.ReqestingVariable;

                    cbAnswers.ItemsSource = e.Variable.Domain.Values;
                    if (cbAnswers.Items.Count > 0)
                        cbAnswers.SelectedIndex = 0;

                    AddMessage(new ConsultationMessage(MessageType.Question,
                        e.Variable.QuestionText));

                    SetUiEnabled(true);
                }));

            valueGetEvent.WaitOne();
            valueGetEvent.Reset();

            e.Value = currentValue;
        }

        private void AddMessage(ConsultationMessage msg)
        {
            spMessages.Children.Add(msg);
            svMessages.ScrollToEnd();
        }

        private void btAnswer_Click(object sender, RoutedEventArgs e)
        {
            OnAnswer();
        }

        private void OnAnswer()
        {
            AddMessage(new ConsultationMessage(MessageType.Answer, cbAnswers.Text));

            if (State == ConsultationState.ChoosingGoal)
            {
                ExpertSystem.GoalVariable = cbAnswers.SelectedItem as Variable;
                Task.Run(() => lim.DeduceGoalVariable(ExpertSystem.GoalVariable));
            }
            else if (State == ConsultationState.ReqestingVariable)
            {
                currentValue = cbAnswers.SelectedItem as DomainValue;
                valueGetEvent.Set();
            }

            cbAnswers.ItemsSource = null;
            SetUiEnabled(false);
        }

		private void btShowExplanation_Click(object sender, RoutedEventArgs e)
		{
			ExplanationWindow dlg = new ExplanationWindow(lim.workingMemory)
			{
				Owner = this
			};

			dlg.ShowDialog();
		}

		private void btNewConsult_Click(object sender, RoutedEventArgs e)
		{
			StartNewConsultation();
		}
	}
}
