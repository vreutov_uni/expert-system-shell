﻿using ExpertSystemShell.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExpertSystemShell.Ui.Dialogs.Consultation
{
    /// <summary>
    /// Interaction logic for ExplanationWindow.xaml
    /// </summary>
    public partial class ExplanationWindow : Window
    {
        WorkingMemory workingMemory;
		bool isExpanded;

		public ExplanationWindow(WorkingMemory workingMemory)
        {
            InitializeComponent();
            this.workingMemory = workingMemory;
			isExpanded = false;

			lvVars.ItemsSource = workingMemory.TrueFacts;
            InitializeTreeView();
        }

        private void InitializeTreeView()
        {
			if (workingMemory.GoalVariable != null)
				InitializeTreeViewItem(workingMemory.GoalVariable);
        }

        private void InitializeTreeViewItem(Variable variable, TreeViewItem parent = null)
        {
			string header = "Цель: " + variable.Name;

			Fact varFact = workingMemory.TrueFacts.FirstOrDefault(f => f.Variable == variable);
			if (varFact != null)
			{
				header += " = " + varFact.DomainValue.Name;
			}
			else
			{
				header += " (не удалось вывести)";
			}

			if (variable.Type == VariableType.Requested)
				header += " (запрошена у пользователя)";

			TreeViewItem treeViewItem = new TreeViewItem
            {
                Header = header
			};

            if (parent != null)
                parent.Items.Add(treeViewItem);
            else
                tvRules.Items.Add(treeViewItem);

			try
			{
				Rule rule = workingMemory.InferenceDictionary[variable];

				treeViewItem.Items.Add(new TreeViewItem
				{
					Header = rule.ToString
				});

				foreach (var fact in rule.Premise)
				{
					InitializeTreeViewItem(fact.Variable, treeViewItem);
				}
			}
			catch (KeyNotFoundException) { }
        }

		private void expCollapseLink_Click(object sender, RoutedEventArgs e)
		{
			isExpanded = !isExpanded;

			expCollapseText.Text = isExpanded ?
				"(скрыть все)" : "(раскрыть все)";

			SetTreeViewItems(tvRules, isExpanded);
		}

		void SetTreeViewItems(object obj, bool expand)
		{
			if (obj is TreeViewItem)
			{
				((TreeViewItem)obj).IsExpanded = expand;
				foreach (object obj2 in ((TreeViewItem)obj).Items)
					SetTreeViewItems(obj2, expand);
			}
			else if (obj is ItemsControl)
			{
				foreach (object obj2 in ((ItemsControl)obj).Items)
				{
					if (obj2 != null)
					{
						SetTreeViewItems(((ItemsControl)obj).ItemContainerGenerator.ContainerFromItem(obj2), expand);

						TreeViewItem item = obj2 as TreeViewItem;
						if (item != null)
							item.IsExpanded = expand;
					}
				}
			}
		}
	}
}
