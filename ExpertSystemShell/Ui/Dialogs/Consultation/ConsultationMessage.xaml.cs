﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExpertSystemShell.Ui.Dialogs.Consultation
{
    public enum MessageType { Answer, Question, Error, Result }

    /// <summary>
    /// Interaction logic for ConsultationMessage.xaml
    /// </summary>
    public partial class ConsultationMessage : UserControl
    {
        private MessageType msgType;

        public MessageType MsgType
        {
            get => msgType;
            private set
            {
                switch (value)
                {
                    case MessageType.Answer:
                        brdMessage.Background = ColorFromHex("#d0dfef");
                        HorizontalAlignment = HorizontalAlignment.Right;
                        break;
                    case MessageType.Question:
                        brdMessage.Background = ColorFromHex("#ffffff");
                        HorizontalAlignment = HorizontalAlignment.Left;
                        break;
                    case MessageType.Error:
                        brdMessage.Background = ColorFromHex("#efd0d0");
                        HorizontalAlignment = HorizontalAlignment.Left;
                        break;
                    case MessageType.Result:
                        brdMessage.Background = ColorFromHex("#d3efd0");
                        HorizontalAlignment = HorizontalAlignment.Left;
                        break;
                    default:
                        break;
                }

                msgType = value;
            }
        }

        public ConsultationMessage(MessageType msgType, string text)
        {
            InitializeComponent();
            MsgType = msgType;
            tbMessage.Text = text;
        }

        SolidColorBrush ColorFromHex(string hex) =>
            (SolidColorBrush)(new BrushConverter().ConvertFrom(hex));
    }
}
