﻿using ExpertSystemShell.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using WPF.JoshSmith.ServiceProviders.UI;

namespace ExpertSystemShell.Ui.Dialogs
{
    /// <summary>
    /// Interaction logic for DomainDialog.xaml
    /// </summary>
    public partial class DomainDialog : Window
    {
        public Domain Domain { get; set; }

        private DispatcherTimer nameChangedTimer;

        ListViewDragDropManager<DomainValue> valuesDragManager;

        public DomainDialog(ExpertSystem expertSystem, Domain domain = null)
        {
            Domain = domain?.MakeCopy() ?? new Domain(expertSystem);

            InitializeComponent();

            _this.Title = (domain == null) ? "Создание домена" : "Редактирование домена";
            
            lbDomainValues.ItemsSource = Domain.Values;

            nameChangedTimer = new DispatcherTimer
            {
                Interval = TimeSpan.FromSeconds(1)
            };

            nameChangedTimer.Tick += (sender, args) => OnNameChanged();

            valuesDragManager = new ListViewDragDropManager<DomainValue>(lbDomainValues);

            tbDomainName.Focus();
        }

		public string DomainName
		{
			set
			{
				tbDomainName.Text = value;
			}
		}

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (!lbNameError.IsVisible && Domain.IsValid())
            {
                if (Domain.IsCopy())
                {
                    bool replace = true;

                    if (Domain.HasValuesChanges())
                    {
                        if (Domain.ExpertSystem.Variables.Any(x => x.Domain == Domain.Original))
                        {
                            if (Domain.Original.Values.Any(x => !Domain.Values.Any(v => v.Original == x)
                            && Domain.ExpertSystem.Rules.Any(
                                r => r.Premise.Any(p => p.DomainValue == x) || r.Conclusion.Any(c => c.DomainValue == x))))
                            {
                                var confirmResult = Xceed.Wpf.Toolkit.MessageBox.Show("Внести изменения в текущий домен невозможно: было удалено значение, которое используется по крайней мере в одном правиле. Сохранить изменения как копию текущего домена?",
                                         "Подтверждение", MessageBoxButton.OKCancel);
                                if (confirmResult == MessageBoxResult.OK) replace = false;
                                else return;
                            }
                            else
                            {
								Style style = new Style();
								style.Setters.Add(new Setter(Xceed.Wpf.Toolkit.MessageBox.YesButtonContentProperty, " Сохранить как копию "));
								style.Setters.Add(new Setter(Xceed.Wpf.Toolkit.MessageBox.NoButtonContentProperty, " Изменить текущий "));
								style.Setters.Add(new Setter(Xceed.Wpf.Toolkit.MessageBox.CancelButtonContentProperty, "Отмена"));

								var confirmResult = Xceed.Wpf.Toolkit.MessageBox.Show("Текущий домен используется как минимум в одной переменной. Сохранить изменения как копию текущего домена? Иначе изменения ",
									"Подтверждение", MessageBoxButton.YesNoCancel, style);

                                if (confirmResult == MessageBoxResult.Cancel) return;
                                else if (confirmResult == MessageBoxResult.Yes) replace = false;
                            }
                        }
                    }

                    if (replace)
                        Domain.ReplaceOriginal();
                    else
                    {
                        Domain.MakeOriginal();
                        string newName = Domain.Name + " Копия";
                        int i = 1;
                        while (Domain.ExpertSystem.Domains.Any(x => x.Name.Equals(newName, StringComparison.OrdinalIgnoreCase)))
                        {
                            newName += i.ToString();
                            i++;
                        }
                        Domain.Name = newName;
                        Domain.ExpertSystem.Domains.Add(Domain);
                    }
                }
                else
                {
                    Domain.ExpertSystem.Domains.Add(Domain);
                }

                DialogResult = true;
            }
            else
            {
                List<string> errors = new List<string>();

                if (lbNameError.IsVisible) errors.Add(lbNameError.Content.ToString() + ".");
                if (Domain.Name.Length == 0) errors.Add("Имя домена не может быть пустым.");
                if (Domain.Values.Count == 0) errors.Add("Множество допустимых значений домена не может быть пустым.");

				Xceed.Wpf.Toolkit.MessageBox.Show(string.Join(Environment.NewLine, errors), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            // Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (DialogResult is true) return;

            if (Domain.HasChanges())
            {
                var confirmResult = Xceed.Wpf.Toolkit.MessageBox.Show("В домен были внесены изменения, вы уверены, что хотите выйти без сохранения?",
                                     "Подтверждение", MessageBoxButton.YesNo);

                if (confirmResult == MessageBoxResult.No)
                    e.Cancel = true;
            }
        }

        private void OnEmptyValue()
        {
			Xceed.Wpf.Toolkit.MessageBox.Show("Значение не может быть пустым");
        }

        private void OnDuplicateValue(string duplicate)
        {
			Xceed.Wpf.Toolkit.MessageBox.Show($"Значение '{duplicate}' уже существует");
        }

        private void btnAddValue_Click(object sender, RoutedEventArgs e)
        {
            OnAddValue();
        }

        private void OnAddValue()
        {
            try
            {
                var value = Domain.AddValue(tbDomainValueName.Text.Trim());
                lbDomainValues.SelectedItem = value;
                tbDomainValueName.Focus();
				tbDomainValueName.SelectAll();
			}
            catch (ValueEmpty) { OnEmptyValue(); }
            catch (DomainValueExists ex) { OnDuplicateValue(ex.Value); }
        }

        private void btnDeleteValue_Click(object sender, RoutedEventArgs e)
        {
            OnRemoveValue();
        }

        private void OnRemoveValue()
        {
            int index = lbDomainValues.SelectedIndex;
            Domain.Values.Remove(lbDomainValues.SelectedValue as DomainValue);

            if (index < lbDomainValues.Items.Count - 1)
                lbDomainValues.SelectedIndex = index;
            else if (lbDomainValues.Items.Count != 0)
                lbDomainValues.SelectedIndex = lbDomainValues.Items.Count - 1;
        }

        private void btnEditValue_Click(object sender, RoutedEventArgs e)
        {
            OnEditValue();
        }

        private void OnEditValue()
        {
            try
            {
                var value = Domain.Values[lbDomainValues.SelectedIndex];
                value.Name = tbDomainValueName.Text.Trim();

				if (Domain.Original != null)
				{
					var originalValue = Domain.Original.Values.Where(x => x.IsEqualTo(value)).FirstOrDefault();
					if (originalValue != null)
					{
						int index = lbDomainValues.SelectedIndex;
						Domain.Values[index] = originalValue.MakeCopy();
						lbDomainValues.SelectedItem = Domain.Values[index];
					}
				}

                tbDomainValueName.Focus();
				tbDomainValueName.SelectAll();
			}
            catch (ValueEmpty) { OnEmptyValue(); }
            catch (DomainValueExists ex) { OnDuplicateValue(ex.Value); }
        }

        private void tbDomainName_TextChanged(object sender, TextChangedEventArgs e)
        {
            nameChangedTimer?.Start();
        }

        private void OnNameChanged()
        {
            try
            {
                Domain.Name = tbDomainName.Text.Trim();
                lbNameError.Visibility = Visibility.Hidden;
            }
            catch (NameExists) { lbNameError.Visibility = Visibility.Visible; }
        }

        private void tbDomainValueName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (Keyboard.Modifiers == ModifierKeys.Control)
                    OnEditValue();
                else
                    OnAddValue();

                e.Handled = true;
            }
        }

        private void lbDomainValues_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            MessageBox.Show("hmmm");
        }

        private void lbDomainValues_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete && lbDomainValues.SelectedItem != null)
            {
                OnRemoveValue();
                e.Handled = true;
            }
        }
    }
}
