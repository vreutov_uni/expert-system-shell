﻿using ExpertSystemShell.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using WPF.JoshSmith.ServiceProviders.UI;

namespace ExpertSystemShell.Ui.Dialogs
{
    /// <summary>
    /// Interaction logic for RulesDialog.xaml
    /// </summary>
    public partial class RuleDialog : Window
    {
        public Rule Rule { get; set; }

        private Rule selectedRule;
        private DispatcherTimer nameChangedTimer;
        ListViewDragDropManager<Fact> premiseDragManager;
        ListViewDragDropManager<Fact> conclusionDragManager;

        public RuleDialog(ExpertSystem expertSystem, Rule rule, bool makeNew)
        {
            Rule = makeNew ? new Rule(expertSystem) : rule.MakeCopy();
            selectedRule = rule;

            InitializeComponent();

            Title = (rule == null) ? "Создание правила" : "Редактирование правила";

            nameChangedTimer = new DispatcherTimer
            {
                Interval = TimeSpan.FromSeconds(1)
            };

            nameChangedTimer.Tick += (sender, args) => OnNameChanged();

            premiseDragManager = new ListViewDragDropManager<Fact>(lvPremises);
            conclusionDragManager = new ListViewDragDropManager<Fact>(lvConclusions);

            premiseDragManager.ProcessDrop += PremiseDragManager_ProcessDrop;
            conclusionDragManager.ProcessDrop += ConclusionDragManager_ProcessDrop;

            tbRuleName.Focus();
        }

        private void ConclusionDragManager_ProcessDrop(object sender, ProcessDropEventArgs<Fact> e)
        {
            if (e.OldIndex > -1)
            {
                Rule.Conclusion.Move(e.OldIndex, e.NewIndex);
            }
        }

        private void PremiseDragManager_ProcessDrop(object sender, ProcessDropEventArgs<Fact> e)
        {
            if (e.OldIndex > -1)
            {
                Rule.Premise.Move(e.OldIndex, e.NewIndex);
            }
        }

        private void OnNameChanged()
        {
            try
            {
                Rule.Name = tbRuleName.Text.Trim();
                lbNameError.Visibility = Visibility.Hidden;
            }
            catch (NameExists) { lbNameError.Visibility = Visibility.Visible; }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (!lbNameError.IsVisible && Rule.IsValid())
            {
                if (Rule.IsCopy())
                {
                    Rule.ReplaceOriginal();
                }
                else
                {
                    if (selectedRule != null)
                        Rule.ExpertSystem.Rules.Insert(Rule.ExpertSystem.Rules.IndexOf(selectedRule) + 1, Rule);
                    else
                        Rule.ExpertSystem.Rules.Add(Rule);
                }

                DialogResult = true;
            }
            else
            {
                List<string> errors = new List<string>();

                if (lbNameError.IsVisible) errors.Add(lbNameError.Content.ToString() + ".");
                if (Rule.Name.Length == 0) errors.Add("Имя правила не может быть пустым.");
                if (Rule.Premise.Count == 0) errors.Add("Посылка правила должна содержать по крайней мере один факт.");
                if (Rule.Conclusion.Count == 0) errors.Add("Заключение правила должно содержать по крайней мере один факт.");

				Xceed.Wpf.Toolkit.MessageBox.Show(string.Join(Environment.NewLine, errors), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (DialogResult == true) return;

            if (Rule.HasChanges())
            {
                var confirmResult = Xceed.Wpf.Toolkit.MessageBox.Show("В правило были внесены изменения, вы уверены, что хотите выйти без сохранения?",
                                     "Подтверждение", MessageBoxButton.YesNo);

                if (confirmResult == MessageBoxResult.No)
                    e.Cancel = true;
            }
        }

        private void tbRuleName_TextChanged(object sender, TextChangedEventArgs e)
        {
            nameChangedTimer?.Start();
        }

        private void OnDuplicateFact(string duplicate, bool isPremise)
        {
            string collName = isPremise ? "Посылка" : "Заключение";
			Xceed.Wpf.Toolkit.MessageBox.Show($"{collName} уже содержит факт с переменной '{duplicate}'");
        }

        private void btnAddPremise_Click(object sender, RoutedEventArgs e)
        {
            FactDialog dlg = new FactDialog(Rule.ExpertSystem, Rule, true)
            {
                Owner = this
            };
            dlg.ShowDialog();
        }

        private void btnEditPremise_Click(object sender, RoutedEventArgs e)
        {
            FactDialog dlg = new FactDialog(Rule.ExpertSystem, Rule, true, lvPremises.SelectedItem as Fact)
            {
                Owner = this
            };
            dlg.ShowDialog();
        }

        private void btnRemovePremise_Click(object sender, RoutedEventArgs e)
        {
            Rule.Premise.Remove(lvPremises.SelectedItem as Fact);
        }

        private void btnAddConcl_Click(object sender, RoutedEventArgs e)
        {
            FactDialog dlg = new FactDialog(Rule.ExpertSystem, Rule, false)
            {
                Owner = this
            };
            dlg.ShowDialog();
        }

        private void btnEditConcl_Click(object sender, RoutedEventArgs e)
        {
            FactDialog dlg = new FactDialog(Rule.ExpertSystem, Rule, false, lvConclusions.SelectedItem as Fact)
            {
                Owner = this
            };
            dlg.ShowDialog();
        }

        private void btnRemoveConcl_Click(object sender, RoutedEventArgs e)
        {
            Rule.Conclusion.Remove(lvConclusions.SelectedItem as Fact);
        }
    }
}
