﻿using ExpertSystemShell.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExpertSystemShell.Ui.Dialogs
{
    /// <summary>
    /// Interaction logic for FactDialog.xaml
    /// </summary>
    public partial class FactDialog : Window
    {
        public ExpertSystem ExpertSystem { get; set; }
        public Rule Rule { get; set; }
        public Fact Fact { get; set; }

        private bool isPremise;

        public FactDialog(ExpertSystem expertSystem, Rule rule, bool isPremise, Fact fact = null)
        {
            Fact = fact?.MakeCopy() ?? new Fact();
            this.isPremise = isPremise;

            ExpertSystem = expertSystem;
            Rule = rule;

            InitializeComponent();

            cbVariable.ItemsSource = isPremise ? ExpertSystem.Variables : ExpertSystem.Variables.Where(x => x.Type != VariableType.Requested);
			if (Fact.Variable == null) cbVariable.SelectedIndex = 0;

			string strItem = isPremise ? "посылки" : "заключения";
            Title = (fact == null) ? "Добавление факта " + strItem : "Редактирование факта " + strItem;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (Fact.IsValid())
            {
                if (Fact.IsCopy())
                {
                    Fact.ReplaceOriginal();
                }
                else
                {
                    try
                    {
                        if (isPremise)
                            Rule.AddPremise(Fact);
                        else
                            Rule.AddConclusion(Fact);
                    }
                    catch (FactExists ex)
                    {
                        string target = isPremise ? "Посылка" : "Заключение";
						Xceed.Wpf.Toolkit.MessageBox.Show($"{target} уже содержит факт с переменной '{ex.Name}'.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }
                }

                DialogResult = true;
            }
            else
            {
                List<string> errors = new List<string>();

                if (Fact.Variable == null) errors.Add("Необходимо задать переменную.");
                if (Fact.DomainValue == null) errors.Add("Необходимо задать значение переменной.");

				Xceed.Wpf.Toolkit.MessageBox.Show(string.Join(Environment.NewLine, errors), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            VariableDialog dlg = new VariableDialog(ExpertSystem)
            {
                Owner = this
            };

            if (dlg.ShowDialog() is true)
            {
                if (!isPremise)
                    cbVariable.ItemsSource = ExpertSystem.Variables.Where(x => x.Type != VariableType.Requested);
                
                cbVariable.SelectedItem = dlg.Variable;
				cbValue.SelectedIndex = 0;
			}
        }

        private void thisWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (DialogResult is true) return;

            if (Fact.HasChanges())
            {
                var confirmResult = Xceed.Wpf.Toolkit.MessageBox.Show("В факт были внесены изменения, вы уверены, что хотите выйти без сохранения?",
                                     "Подтверждение", MessageBoxButton.YesNo);

                if (confirmResult == MessageBoxResult.No)
                    e.Cancel = true;
            }
        }

		private void cbVariable_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (Fact.DomainValue == null) cbValue.SelectedIndex = 0;
		}
	}
}
