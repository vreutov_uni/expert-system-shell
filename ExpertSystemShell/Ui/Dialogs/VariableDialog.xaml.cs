﻿using ExpertSystemShell.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ExpertSystemShell.Ui.Dialogs
{
    /// <summary>
    /// Interaction logic for VariableDialog.xaml
    /// </summary>
    public partial class VariableDialog : Window
    {
        public Variable Variable { get; set; }

        private DispatcherTimer nameChangedTimer;

        public VariableDialog(ExpertSystem expertSystem, Variable variable = null)
        {
            Variable = variable?.MakeCopy() ?? new Variable(expertSystem);

            InitializeComponent();

            thisWindow.Title = (variable == null) ? "Создание переменной" : "Редактирование переменной";

            cbDomains.ItemsSource = expertSystem.Domains;

            var enumType = typeof(VariableType);

            rbStack.Children.Clear();

			int startTabIndex = 3;
            foreach (var type in (VariableType[])Enum.GetValues(typeof(VariableType)))
            {
				RadioButton rb = new RadioButton
				{
					Content = Variable.GetTypeDescription(type),
					IsChecked = type == Variable.Type,
					Tag = type,
					TabIndex = startTabIndex
				};

				++startTabIndex;
				rb.Checked += OnRadioTypeChanged;
                rbStack.Children.Add(rb);
            }

            nameChangedTimer = new DispatcherTimer
            {
                Interval = TimeSpan.FromSeconds(1)
            };

            nameChangedTimer.Tick += (sender, args) => OnNameChanged();
            Variable.PropertyChanged += Variable_PropertyChanged;

            AfterTypeChanged();
            tbVariableName.Focus();
        }

        private void OnRadioTypeChanged(object sender, RoutedEventArgs e)
        {
            var rb = sender as RadioButton;
            Variable.Type = (VariableType)rb.Tag;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (!lbNameError.IsVisible && Variable.IsValid())
            {
                if (Variable.Type == VariableType.Deducible)
                    Variable.QuestionText = default(string);

                if (Variable.IsCopy())
                {
                    bool replace = true;

                    if (Variable.Domain != Variable.Original.Domain
                        && Variable.ExpertSystem.Rules.Any(
                            r => r.Premise.Any(p => p.Variable == Variable.Original)
                            || r.Conclusion.Any(c => c.Variable == Variable.Original)))
                    {
                        replace = false;

                        var result = Xceed.Wpf.Toolkit.MessageBox.Show(
                            "Изменение домена переменной, которая используется в правилах, невозможно. Сохранить изменения как копию текущей переменной?",
                                         "Подтверждение", MessageBoxButton.OKCancel);

                        if (result == MessageBoxResult.Cancel) return;
                    }

                    if (replace)
                        Variable.ReplaceOriginal();
                    else
                    {
                        Variable.MakeOriginal();

                        string newName = Variable.Name + " Копия";
                        int i = 1;
                        while (Variable.ExpertSystem.Variables.Any(x => x.Name.Equals(newName, StringComparison.OrdinalIgnoreCase)))
                        {
                            newName += i.ToString();
                            i++;
                        }

                        Variable.Name = newName;
                        Variable.ExpertSystem.Variables.Add(Variable);
                    }
                    
                }
                else
                    Variable.ExpertSystem.Variables.Add(Variable);

                DialogResult = true;
            }
            else
            {
                List<string> errors = new List<string>();

                if (lbNameError.IsVisible) errors.Add(lbNameError.Content.ToString() + ".");
                if (Variable.Name.Length == 0) errors.Add("Имя переменной не может быть пустым.");
                if (Variable.Domain == null) errors.Add("Необходимо задать домен допустимых значений.");

				Xceed.Wpf.Toolkit.MessageBox.Show(string.Join(Environment.NewLine, errors), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void Variable_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Type")
            {
                AfterTypeChanged();
            }
        }

        private void AfterTypeChanged()
        {
            if (Variable.Type == VariableType.Deducible)
                tbQuestion.Background = Brushes.LightGray;
            else
                tbQuestion.Background = Brushes.White;

            tbQuestion.IsEnabled = Variable.Type != VariableType.Deducible;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            // Close();
        }

        private void thisWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (DialogResult == true) return;

            if (Variable.HasChanges())
            {
                var confirmResult = Xceed.Wpf.Toolkit.MessageBox.Show("В переменную были внесены изменения, вы уверены, что хотите выйти без сохранения?",
                                     "Подтверждение", MessageBoxButton.YesNo);

                if (confirmResult == MessageBoxResult.No)
                    e.Cancel = true;
            }
        }
        
        private void btnAddDomain_Click(object sender, RoutedEventArgs e)
        {
            DomainDialog dlg = new DomainDialog(Variable.ExpertSystem)
            {
                Owner = this,
			};

			if (!Variable.Name.StartsWith("Variable"))
				dlg.DomainName = Variable.Name;

            if (dlg.ShowDialog() is true)
            {
                cbDomains.SelectedValue = dlg.Domain;
            }
        }

        private void tbVariableName_TextChanged(object sender, TextChangedEventArgs e)
        {
            nameChangedTimer?.Start();
        }
        
        private void OnNameChanged()
        {
			nameChangedTimer?.Stop();

			try
            {
                Variable.Name = tbVariableName.Text.Trim();
                lbNameError.Visibility = Visibility.Hidden;
            }
            catch (NameExists) { lbNameError.Visibility = Visibility.Visible; }
        }
    }
}
