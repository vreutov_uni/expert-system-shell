﻿using ExpertSystemShell.Model;
using ExpertSystemShell.Ui.Dialogs;
using System.Windows;
using System.Windows.Controls;

namespace ExpertSystemShell.Ui
{
    /// <summary>
    /// Interaction logic for DomainsControl.xaml
    /// </summary>
    public partial class DomainsControl : UserControl
    {
        private ExpertSystem expertSystem;

        public ExpertSystem ExpertSystem {
            get { return expertSystem; }
            set {
                expertSystem = value;
                lbDomains.ItemsSource = expertSystem.Domains;
            }
        }

        public DomainsControl()
        {
            InitializeComponent();
        }

        private void btnAddDomain_Click(object sender, RoutedEventArgs e)
        {
            DomainDialog dlg = new DomainDialog(ExpertSystem)
            {
                Owner = Application.Current.MainWindow
            };
            dlg.ShowDialog();
        }

        private void btnEditDomain_Click(object sender, RoutedEventArgs e)
        {
            OnEditDomain();
        }

        private void OnEditDomain()
        {
            DomainDialog dlg = new DomainDialog(ExpertSystem, lbDomains.SelectedItem as Domain)
            {
                Owner = Application.Current.MainWindow
            };
            dlg.ShowDialog();
        }

        private void btnRemoveDomain_Click(object sender, RoutedEventArgs e)
        {
            if (!ExpertSystem.Remove(lbDomains.SelectedItem as Domain))
            {
                var confirmResult = Xceed.Wpf.Toolkit.MessageBox.Show("Удаление домена приведет к удалению всех переменных и правил, в которых он используется. Вы уверены, что хотите удалить домен?",
                                     "Подтверждение", MessageBoxButton.YesNo);

                if (confirmResult == MessageBoxResult.Yes)
                {
                    ExpertSystem.Remove(lbDomains.SelectedItem as Domain, true);
                }
            }
        }

        private void lbDomains_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (((FrameworkElement)e.OriginalSource).DataContext != null)
                OnEditDomain();
        }
    }
}
