﻿using ExpertSystemShell.Model;
using ExpertSystemShell.Ui.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPF.JoshSmith.ServiceProviders.UI;

namespace ExpertSystemShell.Ui
{
    /// <summary>
    /// Interaction logic for RulesControl.xaml
    /// </summary>
    public partial class RulesControl : UserControl
    {
        private ExpertSystem expertSystem;
		ListViewDragDropManager<Rule> rulesDragManager;

		public ExpertSystem ExpertSystem
        {
            get { return expertSystem; }
            set
            {
                expertSystem = value;
                lvRules.ItemsSource = expertSystem.Rules;
            }
        }

        public RulesControl()
        {
            InitializeComponent();

			rulesDragManager = new ListViewDragDropManager<Rule>(lvRules);
		}

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            RuleDialog dlg = new RuleDialog(ExpertSystem, lvRules.SelectedItem as Rule, true)
            {
                Owner = Application.Current.MainWindow
            };
            dlg.ShowDialog();

			lvRules.SelectedItem = dlg.Rule;
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            OnEditRule();
        }

        private void OnEditRule()
        {
            RuleDialog dlg = new RuleDialog(ExpertSystem, lvRules.SelectedItem as Rule, false)
            {
                Owner = Application.Current.MainWindow
            };
            dlg.ShowDialog();
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            ExpertSystem.Remove(lvRules.SelectedItem as Rule);
        }

        private void lvRules_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (((FrameworkElement) e.OriginalSource).DataContext != null)
            {
                OnEditRule();
            }
        }
    }
}
