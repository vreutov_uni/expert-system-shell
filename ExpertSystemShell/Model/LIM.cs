﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell.Model
{
    public class LIM
    {
        public ExpertSystem ExpertSystem { get; private set; }

        public WorkingMemory workingMemory;

        public LIM(ExpertSystem expertSystem)
        {
            ExpertSystem = expertSystem;
            InitializeMemory();
        }

        private void InitializeMemory()
        {
            workingMemory = new WorkingMemory
            {
                UntriggeredRules = ExpertSystem.Rules.ToList()
            };
        }

        private DomainValue RequestVariable(Variable variable)
        {
            VariableRequestEventArgs ea = new VariableRequestEventArgs { Variable = variable };

            VariableRequested?.Invoke(this, ea);

            if (ea.Value == null)
                throw new ArgumentNullException();

            return ea.Value;
        }

        public event EventHandler<VariableRequestEventArgs> VariableRequested;
        public event EventHandler<VariableDeducedEventArgs> VariableDeduced;

        private class InferenceError : Exception
        {
        }

        public void DeduceGoalVariable(Variable variable)
        {
            DomainValue result = null;
            var ea = new VariableDeducedEventArgs();

            workingMemory.GoalVariable = variable;

            try
            {
                result = Deduce(variable);
            }
            catch (InferenceError)
            {
            }
            catch (Exception ex)
            {
                ea.HasError = true;
                ea.Error = ex.ToString();
            }

            ea.Value = result;
            VariableDeduced?.Invoke(this, ea);
        }

        private DomainValue Deduce(Variable variable)
        {
            Fact fact = workingMemory.TrueFacts.FirstOrDefault(x => x.Variable == variable);
            if (fact != null)
                return fact.DomainValue;

            if (variable.Type == VariableType.Requested)
			{
				var result = RequestVariable(variable);
				workingMemory.TrueFacts.Add(new Fact { Variable = variable, DomainValue = result });
				return result;
			}                

            var variableRules = workingMemory.UntriggeredRules
                .Where(r => r.Conclusion.Any(c => c.Variable == variable)).ToList();

            foreach (var rule in variableRules)
            {
                var facts = TryApplyRule(rule);
                if (facts != null)
                {
					workingMemory.TrueFacts.AddRange(facts);
					return facts.First(x => x.Variable == variable).DomainValue;
                }
            }

			return null;
            
			// Variable can not be deduced
            // throw new InferenceError();
        }

        private List<Fact> TryApplyRule(Rule rule)
        {
            workingMemory.UntriggeredRules.Remove(rule);

            foreach (var fact in rule.Premise)
            {
                Fact trueFact = workingMemory.TrueFacts.FirstOrDefault(x => x.Variable == fact.Variable);

                DomainValue value;
                if (trueFact == null)
                {
                    value = Deduce(fact.Variable);
                }
                else
                {
                    value = trueFact.DomainValue;
                }

                if (fact.DomainValue != value)
                {
                    return null;
                }
            }

            foreach (var fact in rule.Conclusion)
                workingMemory.InferenceDictionary[fact.Variable] = rule;

            return rule.Conclusion.ToList();
        }
    }

    public class VariableRequestEventArgs : EventArgs
    {
        public Variable Variable { get; set; }
        public DomainValue Value { get; set; }
    }

    public class VariableDeducedEventArgs : EventArgs
    {
        public DomainValue Value { get; set; }

        public bool HasError { get; set; }
        public string Error { get; set; }
    }
}
