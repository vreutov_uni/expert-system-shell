﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell.Model
{
    [Serializable]
    public class ValueEmpty : Exception
    {
    }

    [Serializable]
    public class DomainValueExists : Exception
    {
        public string Value;
        public DomainValueExists(string value) => Value = value;
    }

    [Serializable]
    public class NameExists : Exception
    {
        public string Name;
        public NameExists(string name) => Name = name;
    }

    [Serializable]
    public class FactExists : Exception
    {
        public string Name;
        public FactExists(string name) => Name = name;
    }
}
