﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell.Model
{
	[Serializable]
	public class Domain : ElementBase<Domain>, IElement
    {
        private string name = "";
        private ObservableCollection<DomainValue> values;

        public string Name {
            get { return name; }
            set
            {
                ExpertSystem?.CheckDomainName(value, original?.Name ?? Name);

                name = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<DomainValue> Values
        {
            get { return values; }
            private set
            {
                values = value;

                foreach (var item in values)
                {
                    item.Domain = this;
                }

                OnPropertyChanged();
            }
        }

        public ExpertSystem ExpertSystem { get; set; }

        private void DefaultInit(ExpertSystem expertSystem)
        {
            ExpertSystem = expertSystem;
            Name = ExpertSystem != null ? $"Domain{ExpertSystem.Domains.Count}" : "";
            Values = new ObservableCollection<DomainValue>();
        }

        public Domain(ExpertSystem expertSystem)
        {
            DefaultInit(expertSystem);
        }

        private Domain()
        {
            DefaultInit(null);
        }

        private Domain(Domain domain, bool linked)
        {
            if (domain != null)
            {
                if (linked) original = domain;

                Name = domain.Name;
                ExpertSystem = domain.ExpertSystem;
                Values = domain.CopyValues(linked);
            }
            else
                throw new ArgumentNullException();
        }

        public ObservableCollection<DomainValue> CopyValues(bool linked)
        {
            return new ObservableCollection<DomainValue>(Values.Select(e => e.MakeCopy(linked)));
        }

        public void CheckDomainValue(string value, string nameToIgnore = "")
        {
            if (value.Length == 0)
                throw new ValueEmpty();

            if (Values.Any(e => e.Name.Equals(value, StringComparison.OrdinalIgnoreCase) && !e.Name.Equals(nameToIgnore)))
                throw new DomainValueExists(value);
        }
        
        public DomainValue AddValue(string value)
        {
            DomainValue domainValue = new DomainValue(this)
            {
                Name = value
            };

            if (IsCopy())
            {
                var originalValue = Original.Values.Where(x => x.IsEqualTo(domainValue)).FirstOrDefault();
                if (originalValue != null)
                    domainValue = originalValue.MakeCopy();
            }

            Values.Add(domainValue);

            return domainValue;
        }

        public override bool IsEqualTo(Domain other)
        {
            return
                Name == other.Name &&
                Values.Count == other.Values.Count &&
                Values.Zip(other.Values, (x, y) => x.IsEqualTo(y)).All(x => x == true);
        }

        public override Domain MakeCopy(bool linked = true)
        {
            return new Domain(this, linked);
        }

        public override bool IsValid()
        {
            return
                ExpertSystem != null &&
                Name.Length > 0 &&
                Values.Count > 0 &&
                Values.All(e => e.IsValid());
        }

        protected override void InnerReplaceOriginal()
        {
            original.Name = Name;

            List<DomainValue> values = new List<DomainValue>();
            foreach (var item in Values)
            {
                if (item.IsCopy())
                {
                    item.ReplaceOriginal();
                    values.Add(item.Original);
                }
                else
                {
                    item.Domain = original;
                    values.Add(item);
                }
            }

            original.Values = new ObservableCollection<DomainValue>(values);
        }

        public override bool IsAtDefaultState()
        {
            return IsEqualTo(new Domain(ExpertSystem));
        }

        public bool HasValuesChanges()
        {
            if (!IsCopy()) return false;

            return Values.Count != Original.Values.Count ||
                Values.Zip(Original.Values, (x, y) => x.IsEqualTo(y)).Any(x => x == false);
        }
    }

	[Serializable]
	public class DomainValue : ElementBase<DomainValue>
    {
        private string name;

        public string Name {
            get { return name; }
            set
            {
                Domain?.CheckDomainValue(value, name);

                name = value;
                OnPropertyChanged();
            }
        }

        public Domain Domain { get; set; }

        public DomainValue(Domain domain) => Domain = domain;

        private DomainValue()
        {
        }

        private DomainValue(DomainValue value, bool linked)
        {
            if (value != null)
            {
                if (linked) original = value;

                Name = value.Name;
                Domain = value.Domain;
            }
            else
                throw new ArgumentNullException();
        }

        public override bool IsEqualTo(DomainValue other)
        {
            return Name == other.Name;
        }

        public override bool IsValid()
        {
            return Name.Length > 0;
        }

        protected override void InnerReplaceOriginal()
        {
            original.Name = Name;
        }

        public override DomainValue MakeCopy(bool linked = true)
        {
            return new DomainValue(this, linked);
        }

        public override bool IsAtDefaultState()
        {
            return IsEqualTo(new DomainValue(Domain));
        }
    }
}
