﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell.Model
{
    public enum VariableType
    {
        [Description("Запрашиваемая")]
        Requested,
        [Description("Выводимая")]
        Deducible,
        [Description("Запрашиваемо-выводимая")]
        RequestedDeducible,
    }

	[Serializable]
	public class Variable : ElementBase<Variable>, IElement
    {
        private string name = "";
        private VariableType type;
        private Domain domain;
        private string questionText = "";

        public string Name
        {
            get { return name; }
            set
            {
                ExpertSystem?.CheckVariableName(value, original?.Name ?? Name);

                name = value;
                OnPropertyChanged();
                OnPropertyChanged("QuestionText");
            }
        }

        public VariableType Type
        {
            get { return type; }
            set
            {
                type = value;
                OnPropertyChanged();
				OnPropertyChanged("TypeDescription");
				OnPropertyChanged("QuestionText");
            }
        }

        public Domain Domain
        {
            get { return domain; }
            set
            {
                domain = value;
                OnPropertyChanged();
            }
        }

        public string QuestionText
        {
            get {
                if (Type == VariableType.Deducible)
                    return default(string);

                if (questionText.Length == 0 && Name.Length != 0)
                    return GenerateQuestionFromName();
                else
                    return questionText;
            }
            set
            {
				questionText = value ?? "";
                OnPropertyChanged();
            }
        }

        public string TypeDescription
        {
            get
            {
                return GetTypeDescription(Type);
            }
        }

        public string GetTypeDescription(VariableType variableType)
        {
            return (typeof(VariableType).GetMember(variableType.ToString())[0]
                .GetCustomAttributes(typeof(DescriptionAttribute), false)[0] as DescriptionAttribute)
                .Description;
        }

        public ExpertSystem ExpertSystem { get; set; }

        private void DefaultInit(ExpertSystem expertSystem)
        {
            ExpertSystem = expertSystem;
            if (ExpertSystem != null)
                Name = $"Variable{ExpertSystem.Variables.Count}";

			Type = VariableType.Deducible;
        }

        public Variable(ExpertSystem expertSystem) => DefaultInit(expertSystem);

        private Variable(Variable variable, bool linked)
        {
            if (variable != null)
            {
                if (linked) original = variable;

                ExpertSystem = original.ExpertSystem;
                Name = variable.Name;
                Type = variable.Type;
                Domain = variable.Domain;
                QuestionText = variable.QuestionText;
            }
        }

        public override bool IsEqualTo(Variable other)
        {
            return
                Name == other.Name &&
                Type == other.Type &&
                Domain == other.Domain &&
                QuestionText == other.QuestionText;
        }

        public override Variable MakeCopy(bool linked = true)
        {
            return new Variable(this, linked);
        }

        public override bool IsValid()
        {
            return
                ExpertSystem != null &&
                Domain != null &&
                Name.Length > 0;
        }

        protected override void InnerReplaceOriginal()
        {
            original.Name = Name;
            original.Domain = Domain;
            original.Type = Type;
            original.QuestionText = QuestionText;
        }

        public override bool IsAtDefaultState()
        {
            return IsEqualTo(new Variable(ExpertSystem));
        }

        private string GenerateQuestionFromName()
        {
            return $"{Name}?";
        }
    }
}
