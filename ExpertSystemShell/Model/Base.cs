﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell.Model
{
	public interface IElement
    {
        string Name { get; set; }
    }

	[Serializable]
	public abstract class Notifier : INotifyPropertyChanged
	{
		[field: NonSerialized]
		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged([CallerMemberName] string prop = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
		}
	}

	[Serializable]
	public abstract class ElementBase<T> : Notifier
	{
        protected T original;

        public T Original => original;

        public bool IsCopy() => original != null;
        public void MakeOriginal() { original = default(T); }
        public bool IsEqualToOriginal() => original == null || IsEqualTo(original);

        public void ReplaceOriginal()
        {
            if (original != null)
                InnerReplaceOriginal();
        }

        public bool HasChanges()
        {
            if (IsCopy())
            {
                return !IsEqualToOriginal();
            }
            else
            {
                return !IsAtDefaultState();
            }
        }

        public abstract T MakeCopy(bool linked = true);
        public abstract bool IsEqualTo(T other);
        public abstract bool IsValid();
        public abstract bool IsAtDefaultState();
        protected abstract void InnerReplaceOriginal();
    }
}
