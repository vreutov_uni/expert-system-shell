﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell.Model
{
	[Serializable]
	public class ExpertSystem : Notifier
	{
		[NonSerialized]
		private string path = "";

		[NonSerialized]
		private LIM _LIM;

		public string Name { get; set; }
		public string Path {
			get
			{
				return path;
			}
			set
			{
				path = value;
				OnPropertyChanged();
				OnPropertyChanged("ExTitle");
			}
		}


		public LIM LIM { get => _LIM; set => _LIM = value; }

		public ObservableCollection<Variable> Variables { get; set; }
		public ObservableCollection<Domain> Domains { get; set; }
		public ObservableCollection<Rule> Rules { get; set; }
		public Variable GoalVariable { get; set; }

		public string ExTitle
		{
			get
			{
				if (Path.Length == 0) return "Новая экспертная система";
				else return System.IO.Path.GetFileName(Path);
			}
		}

		public ExpertSystem()
		{
			Variables = new ObservableCollection<Variable>();
			Domains = new ObservableCollection<Domain>();
			Rules = new ObservableCollection<Rule>();
		}

		private bool InnerRemove<T1, T2>(
			T1 itemToRemove,
			ICollection<T1> collection,
			IEnumerable<T2> dependingItems,
			bool cascade = false) where T2 : IElement
		{
			bool needCascade = dependingItems.Any();

			if (cascade && needCascade)
			{
				foreach (var item in dependingItems.ToList())
					Remove(item, true);
			}

			if (cascade || needCascade == false)
			{
				collection.Remove(itemToRemove);
				return true;
			}
			else
				return false;
		}

		public bool Remove(IElement element, bool cascade = false)
		{
			if (element is Domain)
				return Remove(element as Domain, cascade);
			if (element is Variable)
				return Remove(element as Variable, cascade);
			if (element is Rule)
				return Remove(element as Rule);

			throw new ArgumentException();
		}

		public bool Remove(Domain domain, bool cascade = false)
		{
			return InnerRemove(domain, Domains, GetVariablesWithDomain(domain), cascade);
		}

		public bool Remove(Variable variable, bool cascade = false)
		{
			return InnerRemove(variable, Variables, GetRulesWithVariable(variable), cascade);
		}

		public bool Remove(Rule rule)
		{
			Rules.Remove(rule);
			return true;
		}

		private IEnumerable<Variable> GetVariablesWithDomain(Domain domain)
		{
			return Variables.Where(e => e.Domain == domain);
		}

		private IEnumerable<Rule> GetRulesWithVariable(Variable variable)
		{
			return Rules.Where(
						e => e.Premise.Where(p => p.Variable == variable).Any()
					).Union(Rules.Where(
						e => e.Conclusion.Where(p => p.Variable == variable).Any()));
		}

		private void CheckUniqueName<T>(ICollection<T> collection, string name, string nameToIgnore = "")
			where T : IElement
		{
			if (collection.Any(x => x.Name.Equals(name, StringComparison.OrdinalIgnoreCase) && x.Name != nameToIgnore))
				throw new NameExists(name);
		}

		public void CheckDomainName(string name, string nameToIgnore = "")
		{
			CheckUniqueName(Domains, name, nameToIgnore);
		}

		public void CheckVariableName(string name, string nameToIgnore = "")
		{
			CheckUniqueName(Variables, name, nameToIgnore);
		}

		public void CheckRuleName(string name, string nameToIgnore = "")
		{
			CheckUniqueName(Rules, name, nameToIgnore);
		}
	}
}
