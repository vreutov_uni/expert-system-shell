﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell.Model
{
    public class WorkingMemory
    {
        public Variable GoalVariable { get; set; }
        public List<Fact> TrueFacts { get; set; }
        public List<Rule> UntriggeredRules { get; set; }

        public List<Rule> TriggeredRules { get; private set; }
        public Stack<Rule> RulesStack { get; private set; }

        public Dictionary<Variable, Rule> InferenceDictionary { get; private set; }

        public WorkingMemory()
        {
            TrueFacts = new List<Fact>();
            UntriggeredRules = new List<Rule>();
            TriggeredRules = new List<Rule>();

            RulesStack = new Stack<Rule>();
            InferenceDictionary = new Dictionary<Variable, Rule>();
        }
    }
}
