﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell.Model
{
	[Serializable]
	public class Rule : ElementBase<Rule>, IElement
    {
        private string name;
        private ObservableCollection<Fact> premise;
        private ObservableCollection<Fact> conclusion;
        private string reason;

        public string Name
        {
            get { return name; }
            set
            {
                ExpertSystem?.CheckRuleName(value, original?.Name ?? Name);

                name = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Fact> Premise
        {
            get { return premise; }
            set
            {
                premise = value;
                OnPropertyChanged();
				OnPropertyChanged("ToString");
            }
        }

        public ObservableCollection<Fact> Conclusion
        {
            get { return conclusion; }
            set
            {
                conclusion = value;
                OnPropertyChanged();
				OnPropertyChanged("ToString");
			}
        }

        public string Reason
        {
            get { return reason; }
            set
            {
                reason = value;
                OnPropertyChanged();
            }
        }

        public ExpertSystem ExpertSystem { get; set; }
        
        public void DefaultInit()
        {
            Name = ExpertSystem != null ? $"Rule{ExpertSystem.Rules.Count}" : "";
            Premise = new ObservableCollection<Fact>();
            Conclusion = new ObservableCollection<Fact>();
            Reason = "";

            PropertyChanged += Rule_PropertyChanged;
        }

        private void Rule_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "ToString")
                OnPropertyChanged("ToString");
        }

        private Rule() => DefaultInit();

        public Rule(ExpertSystem expertSystem)
        {
            ExpertSystem = expertSystem;
            DefaultInit();
        }

        private Rule(Rule rule, bool linked)
        {
            if (rule != null)
            {
                if (linked) original = rule;

                ExpertSystem = rule.ExpertSystem;
                Name = rule.Name;
                Premise = CopyFacts(rule.Premise);
                Conclusion = CopyFacts(rule.Conclusion);
                Reason = rule.Reason;
            }
        }

        private ObservableCollection<Fact> CopyFacts(ObservableCollection<Fact> factCollection)
        {
            return new ObservableCollection<Fact>(factCollection.Select(e => e.MakeCopy(false)));
        }

        public override Rule MakeCopy(bool linked = true)
        {
            return new Rule(this, linked);
        }

        public override bool IsEqualTo(Rule other)
        {
            return Name == other.Name &&
                Reason == other.Reason &&
                Premise.Count == other.Premise.Count &&
                Premise.Zip(other.Premise, (x, y) => x.IsEqualTo(y)).All(x => x == true) &&
                Conclusion.Count == other.Conclusion.Count &&
                Conclusion.Zip(other.Conclusion, (x, y) => x.IsEqualTo(y)).All(x => x == true);
        }

        public override bool IsValid()
        {
            return Name.Length > 0 &&
                Premise.Count > 0 &&
                Conclusion.Count > 0;
        }

        public override bool IsAtDefaultState()
        {
            return IsEqualTo(new Rule(ExpertSystem));
        }

        protected override void InnerReplaceOriginal()
        {
            original.Name = Name;
            original.Reason = Reason;
            original.Premise = CopyFacts(Premise);
            original.Conclusion = CopyFacts(Conclusion);
        }

        public void AddPremise(Fact fact)
        {
            if (Premise.Any(e => e.Variable == fact.Variable))
                throw new FactExists(fact.Variable.Name);

            Premise.Add(fact);
        }

        public void AddConclusion(Fact fact)
        {
            if (Conclusion.Any(e => e.Variable == fact.Variable))
                throw new FactExists(fact.Variable.Name);

            Conclusion.Add(fact);
        }

        public new string ToString
        {
            get
            {
                return "ЕСЛИ " + string.Join(" И ", Premise.Select(e => e.ToString)) + Environment.NewLine + "ТО " + string.Join(" И ", Conclusion.Select(e => e.ToString));
            }
        }
    }

	[Serializable]
	public class Fact : ElementBase<Fact>
    {
        private Variable variable;
        private DomainValue domainValue;

        public Variable Variable
        {
            get { return variable; }
            set
            {
                DomainValue = null;

                variable = value;
                OnPropertyChanged();
            }
        }

        public DomainValue DomainValue
        {
            get { return domainValue; }
            set
            {
                domainValue = value;
                OnPropertyChanged();
            }
        }

        public Fact()
        {
            PropertyChanged += Fact_PropertyChanged;
        }

        private void Fact_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "ToString")
                OnPropertyChanged("ToString");
        }

        private Fact(Fact fact, bool linked)
            : this()
        {
            if (fact != null)
            {
                if (linked) original = fact;

                Variable = fact.Variable;
                DomainValue = fact.DomainValue;
            }
            else
                throw new ArgumentNullException();
        }

        public override bool IsAtDefaultState()
        {
            return IsEqualTo(new Fact());
        }

        public override bool IsEqualTo(Fact other)
        {
            return Variable == other.Variable &&
                DomainValue == other.DomainValue;
        }

        public override bool IsValid()
        {
            return
                Variable != null &&
                DomainValue != null &&
                DomainValue.Domain == Variable.Domain;
        }

        public override Fact MakeCopy(bool linked = true)
        {
            return new Fact(this, linked);
        }

        protected override void InnerReplaceOriginal()
        {
            original.Variable = Variable;
            original.DomainValue = DomainValue;
        }

        public new string ToString => $"{Variable.Name} = {DomainValue?.Name}";
    }
}
