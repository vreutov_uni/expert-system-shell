﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ExpertSystemShell.Model;
using ExpertSystemShell.Ui.Dialogs;
using ExpertSystemShell.Ui.Dialogs.Consultation;
using Microsoft.Win32;
using ThomasJaworski.ComponentModel;

namespace ExpertSystemShell
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ExpertSystem ExpertSystem { get; set; }
		ChangeListener changeListener;

		bool expertSystemChanged;

		public MainWindow()
        {
            ExpertSystem = new ExpertSystem();
			
            InitializeComponent();

			OnExpertSystemChange();
		}

		void UnsetListener()
		{
			if (changeListener != null)
			{
				changeListener.PropertyChanged -= ChangeListener_PropertyChanged;
				changeListener = null;
			}
		}

		void OnExpertSystemChange()
		{
			//UnsetListener();
			//expertSystemChanged = false;

			//if (ExpertSystem != null)
			//{
			//	changeListener = ChangeListener.Create(ExpertSystem);
			//	changeListener.PropertyChanged += ChangeListener_PropertyChanged;
			//}

			GetBindingExpression(TitleProperty).UpdateTarget();

			ucRules.ExpertSystem = ExpertSystem;
			ucVariables.ExpertSystem = ExpertSystem;
			ucDomains.ExpertSystem = ExpertSystem;
		}

		private void ChangeListener_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			expertSystemChanged = true;
		}

		private void mItOpenConsultation_Click(object sender, RoutedEventArgs e)
        {
            ConsultationDialog dlg = new ConsultationDialog(ExpertSystem)
            {
                Owner = Application.Current.MainWindow
            };
            dlg.ShowDialog();
        }

		private void SaveExpertSystem()
		{
			BinaryFormatter formatter = new BinaryFormatter();
			FileStream fs = new FileStream(ExpertSystem.Path, FileMode.OpenOrCreate);

			fs.SetLength(0); // truncate file

			try
			{
				formatter.Serialize(fs, ExpertSystem);
				expertSystemChanged = false;
			}
			catch (SerializationException e)
			{
				Xceed.Wpf.Toolkit.MessageBox.Show($"Не удалось сохранить экспертную систему: {e.Message}", "Ошибка сохранения", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			finally
			{
				fs.Close();
			}
		}

		private void OpenExpertSystem()
		{
			OpenFileDialog dlg = new OpenFileDialog
			{
				Filter = "BIN files (*.bin)|*.bin|All files (*.*)|*.*",
				FilterIndex = 2,
				RestoreDirectory = true,
				Multiselect = false
			};

			if (dlg.ShowDialog() == true)
			{
				string path = dlg.FileName;

				FileStream fs = new FileStream(path, FileMode.Open);
				try
				{
					UnsetListener();

					BinaryFormatter formatter = new BinaryFormatter();
					ExpertSystem = (ExpertSystem)formatter.Deserialize(fs);

					ExpertSystem.Path = path;
					OnExpertSystemChange();
				}
				catch (SerializationException e)
				{
					Xceed.Wpf.Toolkit.MessageBox.Show($"Не удалось загрузить экспертную систему из файла: {e.Message}", "Ошибка сохранения", MessageBoxButton.OK, MessageBoxImage.Error);
				}
				finally
				{
					fs.Close();
				}
			}
		}

		private void cmdSaveAs_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			SaveFileDialog dlg = new SaveFileDialog
			{
				RestoreDirectory = true,
				DefaultExt = ".bin",
				FileName = "ExpertSystem"
			};

			var result = dlg.ShowDialog();

			if (result == true)
			{
				ExpertSystem.Path = dlg.FileName;
				SaveExpertSystem();
			}
		}

		private void cmdSave_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			if (File.Exists(ExpertSystem.Path))
			{
				SaveExpertSystem();
			}
			else
			{
				cmdSaveAs_Executed(sender, e);
			}
		}

		private void cmdOpen_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			if (!CheckChanges()) return;

			OpenExpertSystem();
		}

		private void cmdNew_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			if (!CheckChanges()) return;

			ExpertSystem = new ExpertSystem();
			OnExpertSystemChange();
		}

		private void mItExit_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void thisWindow_Closing(object sender, CancelEventArgs e)
		{
			if (!CheckChanges())
			{
				e.Cancel = true;
			}
		}

		private bool CheckChanges()
		{
			if (expertSystemChanged)
			{
				var result = Xceed.Wpf.Toolkit.MessageBox.Show("Сохранить изменения?", "Подтверждение", MessageBoxButton.YesNoCancel);

				if (result == MessageBoxResult.Cancel)
				{
					return false;
				}
				else if (result == MessageBoxResult.Yes)
				{
					cmdSave_Executed(null, null);
				}
			}

			return true;
		}

		private void mItOpenExplanation_Click(object sender, RoutedEventArgs e)
		{
			if (ExpertSystem.LIM == null)
			{
				Xceed.Wpf.Toolkit.MessageBox.Show("Сначала необходимо начать консультацию.");
				return;
			}

			ExplanationWindow dlg = new ExplanationWindow(ExpertSystem.LIM.workingMemory)
			{
				Owner = this
			};

			dlg.ShowDialog();
		}
	}
}
