﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace ExpertSystemShell
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            EventManager.RegisterClassHandler(typeof(ListView),
                UIElement.MouseDownEvent, new RoutedEventHandler(ListView_MouseDown));
            
            base.OnStartup(e);
        }

        private void ListView_MouseDown(object sender, RoutedEventArgs e)
        {
            if (sender is ListView listView && e is MouseButtonEventArgs ea)
            {
                HitTestResult r = VisualTreeHelper.HitTest(listView, ea.GetPosition(listView));
                if (r.VisualHit.GetType() != typeof(ListBoxItem))
                    listView.UnselectAll();
            }
        }
    }
}
